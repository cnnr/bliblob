# coding: utf-8

namespace :bliblob do
  desc 'Migrate categories\' pics from dumb teaser_picture_link to native uploader'
  task :migrate_catpics, [:confirm_dirty] => [:environment] do |_, args|
    def find_ck_pic(url)
      if url.start_with? '/system/ckeditor_assets/pictures/'
        begin
          Ckeditor::Picture.find(url.split('/')[4])
        rescue
          nil
        end
      end
    end

    quick_and_dirty = args[:confirm_dirty] == 'quick_and_dirty'

    Category.all.each do |category|

      link = category.teaser_picture_link
      category.unset :teaser_picture_link

      next if link.blank?

      category.picture = File.open("#{Rails.root}/public#{link}")
      category.save!

      unless (ck_pic = find_ck_pic(link)).blank? ||
        Category.where(teaser_picture_link: /#{ck_pic.id}/).present? ||
        (!quick_and_dirty &&
          ( Page.where(description: /#{ck_pic.id}/).present? ||
            Category.where(description: /#{ck_pic.id}/).present? ||
            News.where(description: /#{ck_pic.id}/).present?
          )
        )

        ck_pic.destroy
      end
    end
  end

  desc 'Remove unused ckeditor pictures'
  task :remove_unused_ckpics => :environment do
    Ckeditor::Picture.all.each do |picture|
      pic_id = picture.id

      picture.destroy unless
        Category.where(teaser_picture_link: picture.data.url).present? ||
        News.where(picture_link: picture.data.url).present? ||
        Page.where(description: /#{pic_id}/).present? ||
        Category.where(description: /#{pic_id}/).present? ||
        News.where(description: /#{pic_id}/).present?
    end

    Dir.chdir("#{Rails.root}/public/system/ckeditor_assets/pictures/")
    Dir.open("#{Rails.root}/public/system/ckeditor_assets/pictures/").each do |subdir|
      next if %w(.. .).include? subdir
      next unless File.directory? subdir

      begin
        _ = Ckeditor::Picture.find(subdir)
      rescue
        FileUtils.rm_rf subdir
      end
    end
  end
end
