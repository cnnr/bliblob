# coding: utf-8

namespace :cleanup do
  task :cleanup => :environment do
    puts Extensions::Cleanup.cleanup File.readlines(ENV['file']).join
  end
end