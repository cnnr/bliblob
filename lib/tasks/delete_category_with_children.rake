# coding: utf-8

namespace :bliblob do
  desc 'Delete category and all it\'s children; usage: rake "bliblob:delete_category[id, Yes]"'
  task :delete_category, [:victim_id, :confirm] => [:environment] do |t, args|
    def genocide(category, for_real = false)
      if category.categories.present?
        category.categories.each do |child|
          genocide child, for_real
        end
      end

      puts "Category «#{category.name}» killed"
      category.destroy if for_real
    end

    puts "It's name was #{Category.find(args[:victim_id]).name}"
    genocide Category.find(args[:victim_id]), args[:confirm] == 'Yes'
  end
end
