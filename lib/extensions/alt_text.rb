module Extensions
  module AltText
    extend ActiveSupport::Concern

    module ClassMethods
      def alt_text_for(*args)
        self.alt_text_attributes = args.collect &:to_sym
      end
    end

    module InstanceMethods
      def update_alt_text
        self.alt_text_attributes.each do |sym|
          text = self.send(sym)

          alt = "<img alt=\"#{self.name.gsub(/["><]/, '')}\" "
          text.gsub! '<img alt=', '<img_alt='
          text.gsub! '<img ', alt

          text.gsub! '<img_alt=', '<img alt='

          self.send "#{sym}=".to_sym, text
        end
      end
    end

    self.included do
      class_attribute :alt_text_attributes

      before_save :update_alt_text

      extend ClassMethods
      include InstanceMethods
    end
  end
end