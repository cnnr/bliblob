module Extensions
  module Cleanup
    module ClassMethods
      Transformations = {/(style=".*?")/ => '',
                         /<br.?\/>/ => "</p>\n<p>",
                         /<\/?span.?>/ => '',
                         '</div>' => '</p>',
                         '<div' => '<p',
                         /<p.*?>/ => '<p>',
                         /<font.*?>/ => '',
                         '</font>' => ''}

      def cleanup(text)
        text.lines.collect do |line|
          Transformations.each do |pattern, substitute|
            line.gsub! pattern, substitute
          end

          line
        end.join
      end

      def cleanup_markup_for(*args)
        self.symbols = args
      end
    end

    module InstanceMethods
      def cleanup_callback
        if enable_cleanup.present? && enable_cleanup != "0"
          (self.class.symbols || []).each do |sym|
            send "#{sym}=".to_sym, self.class.cleanup(send sym)
          end
        end
      end
    end

    extend ActiveSupport::Concern

    self.included do
      class_attribute :symbols

      attr_accessor :enable_cleanup

      before_validation :cleanup_callback

      include InstanceMethods
    end

    extend ClassMethods
  end
end
