module Extensions
  module OrderByInteger
    extend ActiveSupport::Concern

    self.included do
      field :order_number, type: Integer

      before_save :update_order_number

      scope :ordered_by_order_number, order_by([[:order_number, :asc]])
      default_scope ordered_by_order_number
    end

    def update_order_number
      unless self.order_number.present?
        self.order_number = if self.embedded?
                              self._parent.send(self.class.name.underscore.pluralize).size
                            else
                              self.order_number = self.class.count
                            end
      end
    end
  end
end
