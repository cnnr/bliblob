module Extensions
  module UpdateRoutes
    extend ActiveSupport::Concern

    self.included do
      after_save :update_routes
      after_destroy :update_routes
    end

    def update_routes
      Rails.application.routes_reloader.reload!
    end
  end
end