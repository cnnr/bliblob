module Extensions
  module Teaser
    extend ActiveSupport::Concern

    self.included do
      field :teaser, type: String

      field :teaser_picture_link, type: String
      attr_accessor :teaser_picture

      before_validation do
        if self.teaser_picture.present?
          self.teaser_picture_link = Ckeditor::Picture.create(data: self.teaser_picture).data.url
        end
      end
    end
  end
end