module Extensions
  module Translit
    extend ActiveSupport::Concern

    self.included do
      field :name, type: String
      field :name_translit, type: String

      before_validation :transliterate_all
      validates_presence_of :name, :name_translit
      #validates_uniqueness_of :name, :name_translit

      alias :path :name_translit
      alias :path= :name_translit=
    end

    def transliterate_all
      unless self.name_translit.present?
        transliterate_all!
      end
    end

    def transliterate_all!
      self.name_translit = Translit.transliterate(self.name) if self.name.present?

      if self.respond_to?(:parent) && parent.present?
        parent_level = parent.children
      else
        parent_level = if self.class.respond_to?(:first_level)
                         self.class.first_level
                       elsif self.embedded?
                         self._parent.send(self.class.name.underscore.pluralize)
                       else
                         self.class
                       end
      end

      if parent_level.where(name_translit: self.name_translit).not_in(_id: [self.id]).present?
        names = parent_level.where(name_translit: /\A#{self.name_translit}/).to_a.map{|c| c.name_translit}

        i = 1
        begin
          i += 1
          new_name_translit = "#{self.name_translit}-#{i}"
        end while names.include? new_name_translit

        self.name_translit = new_name_translit
      end
    end

    def self.transliterate(str)
      Russian::transliterate(str.strip).gsub(/[[:punct:]]/, '').gsub(' ', '-').downcase
    end
  end
end
