module Extensions
  module Hide
    def self.included(base)
      base.class_exec do
        field :published, type: Boolean, default: true
        default_scope not_in(:published => [false])
      end
    end

    module InstanceMethods
      def publish
        self.published = true
        save
      end

      def hide
        self.published = false
        save
      end

      def toggle
        self.published = !self.published
        save
      end
    end

    include InstanceMethods
  end
end