module Extensions
  module Meta
    extend ActiveSupport::Concern

    self.included do
      field :meta, type: Hash, default: {}

      before_save :update_meta
    end

    def update_meta
      %w'title keywords description'.each do |field|
        unless self.meta[field].present?
          self.meta[field] = self.name
        end
      end
    end
  end
end