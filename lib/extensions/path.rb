# coding: utf-8
module Extensions
  module Path
    extend ActiveSupport::Concern

    self.included do
      field :slug, type: String
      field :parent_names, type: Array, default: []
      field :subdomain, type: String, default: ''

      before_validation :update_slug
      validates_presence_of :slug

      validate :check_uniqueness_of_slug_plus_region
      validate :check_hierarchy

      before_save :update_parent_names
      after_save :update_children
    end

    def update_slug
      self.slug = parent.present? ? "#{parent.slug}/#{self.name_translit}" : "#{self.name_translit}"
    end

    def update_parent_names
      self.parent_names = []
      _parent = self

      while(_parent.present?)
        self.parent_names.unshift _parent.name
        _parent = _parent.parent
      end
    end

    def update_children
      self.children.each &:save if self.children.present? && (self.changed.include?('slug') || self.changed.include?('name'))
    end

    def parent_slugs
      self.slug.split('/').inject([]) do |slugs, item|
        slugs << "#{slugs.last}/#{item}"
      end
    end

    def get_hierarchy
      if self.parent.present?
        self.parent.get_hierarchy << self.id
      else
        [self.id]
      end
    end

    def check_hierarchy
      if self.parent.present?
        errors[:parent] = 'Нельзя ссылаться на своего ребенка' if self.parent.get_hierarchy.include?(self.id)
      end
    end

    def breadcrumbs
      [self.parent_names, self.parent_slugs]
    end

    def check_uniqueness_of_slug_plus_region
      !self.class.where(slug: self.slug, subdomain: self.subdomain).exists?
    end
  end
end
