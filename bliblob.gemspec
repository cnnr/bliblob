$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem"s version:
require "bliblob/version"

# Describe your  s.add_dependencyand declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "bliblob"
  s.version     = Bliblob::VERSION
  s.authors     = ["Kirill Maksimov", "Andrew Shmelkov"]
  s.email       = ["netoneko@gmail.com", "irratio@gmail.com"]
  s.homepage    = "https://bitbucket.com/sqcat/bliblob"
  s.summary     = "Bliblob is a content management system of Sqcat.com"
  s.description = "Just look at this cutie: http://adventuretime.wikia.com/wiki/Bliblob"

  s.files = Dir["{app,config,db,lib}/**/*"] + ["Rakefile", "Capfile", "Gemfile", "Gemfile.lock"]

  s.add_dependency "rails", "= 3.2.18"
  s.add_dependency "rack-cache", "= 1.2"
  s.add_dependency "coffee-rails", "= 3.2.2"
  s.add_dependency "uglifier", "= 1.2.3"
  s.add_dependency "jquery-rails", "= 2.1.1"
  s.add_dependency "therubyracer", "= 0.12.2"
  s.add_dependency "haml", "= 4.0.4"

  s.add_dependency "bson_ext", "= 1.9.2"
  s.add_dependency "mongoid", "= 2.8.1"
  s.add_dependency "devise", "= 2.2.8"
  s.add_dependency "less-rails", "= 2.3.3"
  s.add_dependency "twitter-bootstrap-rails", "= 2.2.8"
  s.add_dependency "russian", "= 0.6.0"


  s.add_dependency "mini_magick", "= 3.4"
  s.add_dependency "thin", "= 1.6.4"
  s.add_dependency "will_paginate", "= 3.0.3"
  s.add_dependency "will_paginate_mongoid", "= 1.0.5"
  s.add_dependency "bootstrap-will_paginate", "= 0.0.7"
  s.add_dependency "sanitize", "= 2.0.3"

  s.add_dependency "carrierwave", "= 0.8"
  s.add_dependency "carrierwave-mongoid"

  s.add_dependency "newrelic_rpm", "= 3.7.2.195"

  s.add_dependency "unicode_utils", "= 1.0.0"

  s.add_dependency "chosen-rails", "= 0.12.0"
  s.add_dependency "ckeditor", "= 3.7.3"
end
