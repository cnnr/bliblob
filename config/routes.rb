Rails.application.routes.draw do
  mount Ckeditor::Engine => '/ckeditor'

  root to: "index#index"

  get 'sitemap' => 'sitemap#show', format: 'xml'
  match 'robots.txt' => 'robots_txt#index'

  scope 'admin' do
    match '/' => "admin#index"

    resources :categories, :counters, :pages, :text_zones

    resources :item_order do
      collection do
        put '/' => 'item_order#update'
      end
    end


    resources :file_management do
      collection do
        get :images, format: 'json'
        get :attachments, format: 'json'
      end

      collection do
        match :picture_upload
        match :attachment_upload
      end
    end

    resources :news do
      collection do
        get '/' => "news#admin_index"
      end
    end

    resources :orders do
      collection do
        get 'settings' => 'orders#settings', :as => :settings
        post 'settings' => 'orders#update_settings'
        put 'settings' => 'orders#update_settings'
        get 'resend/:id' => 'orders#resend', :as => :resend
      end
    end
  end

  match 'users/sign_up', to: redirect('/')

  devise_for :users, {
      class_name: User,
      module: :devise,
  }

  prefix = Rails.application.routes.catalog_prefix if Rails.application.routes.respond_to?(:catalog_prefix)

  if prefix.present?
    prefix = prefix.prepend('/') unless prefix[0] == '/'
    get prefix => 'catalog#index', as: :catalog
  else
    get '/catalog' => 'catalog#index', as: :catalog
  end

  # TODO enable domain-specific categories here
  get "#{prefix}/(*slug)" => 'catalog#show', :as => :show_category, :constraints => lambda { |req|
    if !req.path.starts_with?('/admin')
      Category.where(slug: req.params[:slug]).exists?
    else
      false
    end
  }

  subdomains_enabled = false # Rails.application.config.subdomains_enabled
  match "/(*slug)" => 'pages#show', :as => :show_page, :constraints => lambda { |req|
    criteria = Page.where(slug: req.params[:slug])

    if subdomains_enabled
      req.params[:subdomain] = req.subdomain == 'www' ? ['www', ''] : [req.subdomain]
      criteria = criteria.in(subdomain: req.params[:subdomain])
    end

    criteria.exists?
  }

end
