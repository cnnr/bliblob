set :application, "bliblob"
set :repository,  "ssh://git@bitbucket.org:sqcat/bliblob.git"
set :branch, "master"

set :scm, :git
# Or: `accurev`, `bzr`, `cvs`, `darcs`, `git`, `mercurial`, `perforce`, `subversion` or `none`

set :deploy_via, :remote_cache
set :deploy_to, "~/deploy"

set :use_sudo, false

ssh_options[:forward_agent] = true

#role :web, :nginx # Your HTTP server, Apache/etc
#role :app, :thin                          # This may be the same as your `Web` server
#role :db, :localhost, :primary => true # This is where Rails migrations will run

# Tell Capistrano the servers it can play with

profiles = {
    demo: {
        user: 'demo', port: 4000, database: 'shopping_development'
    },
}

profile_name = ENV['DEPLOY_PROFILE'] || 'demo'

profile = profiles[profile_name.to_sym]

set :user, profile[:user]

server "sherekhan.sqcat.com", :app, :web, :db, :primary => true

port = profile[:port]

environment = profile[:environment] || 'development'

THIN_SETTINGS = "-a localhost -e #{environment} -s 1 -p #{port} -P /tmp/thin.pid"
GEM_BIN = "~/.gem/bin"
GEM_SETTINGS = "GEM_HOME=~/.gem/"
CD = "cd ~/deploy/current"
SET_PROFILE = "DEPLOY_PROFILE=#{profile_name}"

PRODUCTION_ENV = "MONGOID_HOST=localhost MONGOID_DATABASE=#{profile[:database]} #{SET_PROFILE}"

BUNDLE_INSTALL = "#{CD}; #{GEM_SETTINGS} #{GEM_BIN}/bundle install"
BUNDLE_EXEC = "#{CD}; #{PRODUCTION_ENV} #{GEM_SETTINGS} #{GEM_BIN}/bundle exec"

namespace :deploy do
  desc "Start the Thin processes"
  task :start do
    run "#{BUNDLE_EXEC} thin start #{THIN_SETTINGS}"
  end

  desc "Stop the Thin processes"
  task :stop do
    run "#{BUNDLE_EXEC} thin stop #{THIN_SETTINGS}"
  end

  desc "Restart the Thin processes"
  task :restart do
    run "#{BUNDLE_EXEC} thin restart #{THIN_SETTINGS}"
  end

  task :assets_precompile do
    if environment == 'production'
      run "export RAILS_ENV=production; #{BUNDLE_EXEC} rake --trace assets:clean assets:precompile"
    end
  end

  task :bundle_install do
    run BUNDLE_INSTALL
  end

  before 'deploy:start', 'deploy:bundle_install', 'deploy:assets_precompile'
  before 'deploy:restart', 'deploy:bundle_install', 'deploy:assets_precompile'

end

#namespace :db do
#  task :setup, :except => { :no_release => true } do
#    run "mkdir -p #{shared_path}/config"
#  end
#
#  task :symlink, :except => { :no_release => true } do
#    run "ln -nfs #{shared_path}/config/mongoid.yml #{release_path}/config/mongoid.yml"
#  end
#
#  after "deploy:finalize_update", "db:symlink"
#
#end

TIMESTAMP = "#{profile_name}-#{Time.now.strftime '%Y-%m-%d-%H%M'}"
BACKUP_DIRECTORY = "~/backup/#{TIMESTAMP}"
CD_BACKUP = "cd #{BACKUP_DIRECTORY}"
DROPBOX = "/path/to/dropbox"

namespace :backup do
  task :all do
    run "mkdir -p #{BACKUP_DIRECTORY}"

    run "#{CD_BACKUP}; mongodump -d #{profile[:database]}"
    run "cp -rf ~/deploy/shared #{BACKUP_DIRECTORY}"
    run "rm -rf #{BACKUP_DIRECTORY}/shared/log"

    run "cd ~/backup ; tar cvfj #{TIMESTAMP}.tbz2 #{BACKUP_DIRECTORY}; cp #{TIMESTAMP}.tbz2 #{DROPBOX}"
  end

  task :cleanup do
    run "rm -rf ~/backup"
  end
end

# if you're still using the script/reaper helper you will need
# these http://github.com/rails/irs_process_scripts

# If you are using Passenger mod_rails uncomment this:
# namespace :deploy do
#   task :start do ; end
#   task :stop do ; end
#   task :restart, :roles => :app, :except => { :no_release => true } do
#     run "#{try_sudo} touch #{File.join(current_path,'tmp','restart.txt')}"
#   end
# end
