# Be sure to restart your server when you modify this file.

_BLIBLOB_SESSION = '_bliblob_session'
Rails.application.config.session_store :cookie_store, key: _BLIBLOB_SESSION

# Use the database for sessions instead of the cookie-based default,
# which shouldn't be used to store highly confidential information
# (create the session table with "rails generate session_migration")
# Shopping::Application.config.session_store :active_record_store
