# config: utf-8
# You are free to choose which captcha serves you better
if defined?(Recaptcha)
  Recaptcha.configure do |config|
    config.public_key  = 'replace with your own public key'
    config.private_key = 'replace with your own public key'
    #config.proxy = 'http://myproxy.com.au:8080'
  end
elsif defined?(SimpleCaptcha)
  SimpleCaptcha.setup do |sc|
    #sc.image_magick_path = '/usr/local/bin' # you can check this from console by running: which convert
  end

  ApplicationController.class_eval do
    include SimpleCaptcha::ControllerHelpers

    def verify_recaptcha(options)
      if !(result = simple_captcha_valid?) && options.include?(:model) && options.include?(:message)
        options[:model].errors.add(:captcha, options[:message])
      end

      result
    end
  end

  ApplicationHelper.class_eval do
    include SimpleCaptcha::ViewHelper

    alias_method :recaptcha_tags, :show_simple_captcha
  end
else
  raise "no captcha! sorry"
end
