module CarrierWave
  class SanitizedFile
    alias_method :old_original_filename, :original_filename

    def original_filename
      old_original_filename.split('.').collect do |token|
        Russian::transliterate(token).gsub(/["\#$%&'*+,\/:;<=>?@\[\]_`{|}~]/, '').gsub(' ', '-').downcase
      end.join('.')
    end
  end
end