require File.expand_path('../boot', __FILE__)

# Pick the frameworks you want:
# require "active_record/railtie"
require "action_controller/railtie"
require "action_mailer/railtie"
require "active_resource/railtie"
require "sprockets/railtie"
# # require "rails/test_unit/railtie"

if defined?(Bundler)
  # If you precompile assets before deploying to production, use this line
  Bundler.require(*Rails.groups(:assets => %w(development test)))
  # If you want your assets lazily compiled in production, use this line
  # Bundler.require(:default, :assets, Rails.env)
end

module Bliblob
  class Application < Rails::Application
    attr_accessor :deploy_profile
    attr_accessor :app_version

    config.autoload_paths += %W(#{config.root}/app/models/ckeditor)
    config.autoload_paths += Dir["#{config.root}/lib/**/"]  # include all subdirectories

    Rails.application.routes.class_eval do
      attr_accessor :catalog_prefix
    end

    # don't generate RSpec tests for views and helpers
    config.generators do |g|
      g.view_specs false
      g.helper_specs false
    end

    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.

    # Custom directories with classes and modules you want to be autoloadable.
    # config.autoload_paths += %W(#{config.root}/extras)

    # Only load the plugins named here, in the order given (default is alphabetical).
    # :all can be used as a placeholder for all plugins not explicitly named.
    # config.plugins = [ :exception_notification, :ssl_requirement, :all ]

    config.plugins = []

    deploy_profile = ENV['DEPLOY_PROFILE']

    app_version = `git log -n 1`.lines.to_a[0..2].join
    File.open("#{Rails.root}/public/assets/app_version.txt", "w") do |file|
      file.write app_version
    end if Dir.exists? "#{Rails.root}/public/assets"

    if (profile = deploy_profile).present?
      config.plugins = [profile.to_sym]

      config.autoload_paths += Dir["#{config.root}/vendor/plugins/#{profile}/**/"]

      config.to_prepare do
        ApplicationController.prepend_view_path "#{Rails.application.config.root}/vendor/plugins/#{profile}/app/views/"

        Dir["#{Rails.application.config.root}/vendor/plugins/#{profile}/app/assets/**"].each do |dir|
          Rails.application.assets.prepend_path dir
        end
      end
    end

    # Activate observers that should always be running.
    # config.active_record.observers = :cacher, :garbage_collector, :forum_observer

    # Set Time.zone default to the specified zone and make Active Record auto-convert to this zone.
    # Run "rake -D time" for a list of tasks for finding time zone names. Default is UTC.
    # config.time_zone = 'Central Time (US & Canada)'

    # The default locale is :en and all translations from config/locales/*.rb,yml are auto loaded.
    # config.i18n.load_path += Dir[Rails.root.join('my', 'locales', '*.{rb,yml}').to_s]
    # config.i18n.default_locale = :de

    # Configure the default encoding used in templates for Ruby 1.9.
    config.encoding = "utf-8"

    # Configure sensitive parameters which will be filtered from the log file.
    config.filter_parameters += [:password, :password_confirmation]

    # Enable the asset pipeline
    config.assets.enabled = true

    # Version of your assets, change this if you want to expire all your assets
    #config.assets.version = '1.0'

    #config.assets.compile = true
  end
end
