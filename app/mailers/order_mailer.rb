# coding: utf-8
class OrderMailer < ActionMailer::Base
  default subject: "Новая заявка", from: 'noreply@sqcat.com'

  # TODO add default email
  def order_email(order)
    @order = order
    mail to: OrderSettings.emails(order.order_type), subject: "Заявка от #{@order.name}#{(" (#{@order.company})" if @order.respond_to?(:company) && @order.company.present?)}"
  end
end
