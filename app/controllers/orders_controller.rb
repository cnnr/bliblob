# coding: utf-8
class OrdersController < ApplicationController
  before_filter :authenticate_user!, except: [:new, :create]

  def index
    @orders = Order.desc(:created_at).paginate page: (params[:page] || 1)
  end

  def new
    @page = Page.where(slug: request.path[1..-1]).first
    @order = Order.new
  end

  def show
    @order = Order.find(params[:id])
  end

  def create
    @order = Order.new(params[:order])

    if verify_recaptcha(:model => @order, :message => "Символы с картинки были введены неверно.")  && @order.save
      redirect_to (session[:return_to] || :back), notice: 'Спасибо за заявку! Наши менеджеры скоро свяжутся с вами.'
    else
      render action: 'new'
    end
  end

  def destroy
    @order = Order.find(params[:id])
    @order.destroy

    respond_to do |format|
      format.html { redirect_to orders_path, notice: 'Заявка успешно удалена.' }
      format.json { head :no_content }
    end
  end

  def resend
    @order = Order.find(params[:id])
    OrderMailer.order_email(@order).deliver
    redirect_to (session[:return_to] || :back), notice: 'Повторное оповещение о заказе отправлено'
  end

  def settings

  end

  def update_settings
    OrderSettings.instance.update_attributes!(data: params[:settings])
    redirect_to :settings_orders, notice: 'Настройки почты успешно обновлены'
  end
end
