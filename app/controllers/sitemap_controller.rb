class SitemapController < ApplicationController
  def show
    respond_to do |format|
      format.xml {
        @prefix = if Rails.application.routes.respond_to?(:catalog_prefix) && (@prefix = Rails.application.routes.catalog_prefix).present?
                    "#{@prefix}/"
                  else
                    ""
                  end

        @pages = Page.not_root.distinct(:slug)

        @categories = Category.all.distinct(:slug)

        @news = News.all.distinct(:name_translit)

        @urls = ['']
      }
    end
  end
end