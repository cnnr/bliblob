# coding: utf-8
class CatalogController < ApplicationController
  def index
    @path = []
    @path_translit = []

    path = request.path.gsub /\A\//, ''
    @page = Page.where(slug: path).first
  end

  def show
    @category = Category.where(slug: params[:slug]).first

    raise ActionController::RoutingError.new('Not Found') unless @category.present?

    @subcategories = @category.categories

    @path, @path_translit = @category.breadcrumbs
  end
end
