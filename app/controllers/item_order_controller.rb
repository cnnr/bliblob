# coding: utf-8
class ItemOrderController < ApplicationController
  before_filter :authenticate_user!

  before_filter :load_objects

  def index
    render :edit
  end

  def edit

  end

  def update
    begin
      @objects.each do |object|
        object.order_number = params[:order][object.id.to_s].to_i
        object.save!
      end
    rescue => e
      puts e.inspect
    end

    redirect_to (@category.present? ? edit_item_order_path(@category) : item_order_index_path), notice: 'Порядок отображения успешно обновлен!'
  end

  private
  def load_objects
    @objects = if (id = params[:id]).present?
                 (@category = Category.find(id)).children
               else
                 Category.first_level
               end
  end
end
