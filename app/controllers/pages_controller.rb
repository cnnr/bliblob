# coding: utf-8
class PagesController < ApplicationController
  before_filter :authenticate_user!, :except => :show

  # GET /pages
  # GET /pages.json
  def index
    @pages = Page.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @pages }
    end
  end

  # GET /pages/1
  # GET /pages/1.json
  def show
    # TODO fix subdomain
    # Look at routes.rb and subdomains_enabled
    @page = Page.where(slug: params[:slug]).first #.in(subdomain: params[:subdomain] || [""]).first

    @path, @path_translit = @page.breadcrumbs

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @page }
    end
  end

  # GET /pages/new
  # GET /pages/new.json
  def new
    @page = Page.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @page }
    end
  end

  # GET /pages/1/edit
  def edit
    @page = Page.find(params[:id])
  end

  # POST /pages
  # POST /pages.json
  def create
    @page = Page.new(params[:page])

    if (page_id = params[:page][:page_id]).present?
      @page.page = Page.find(page_id)
    else
      @page.page_id = nil
    end

    respond_to do |format|
      if @page.save
        format.html { redirect_to edit_page_path(@page), notice: 'Страница успешно создана' }
        format.json { render json: @page, status: :created, location: @page }
      else
        format.html { render action: "new" }
        format.json { render json: @page.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /pages/1
  # PUT /pages/1.json
  def update
    @page = Page.find(params[:id])

    page_id = params[:page][:page_id]

    if page_id.present?
      @page.page = Page.find(page_id)
    else
      params[:page][:page_id] = nil
    end

    respond_to do |format|
      if @page.update_attributes(params[:page])
        format.html { redirect_to edit_page_path(@page), notice: 'Страница успешно обновлена' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @page.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /pages/1
  # DELETE /pages/1.json
  def destroy
    @page = Page.find(params[:id])
    @page.destroy

    respond_to do |format|
      format.html { redirect_to pages_url }
      format.json { head :no_content }
    end
  end
end
