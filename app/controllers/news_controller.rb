# coding: utf-8
class NewsController < ApplicationController
  before_filter :authenticate_user!, :except => [:show, :index]

  # GET /news
  # GET /news.json
  def index
    if Rails.configuration.respond_to? :news_per_page
      news_per_page = Rails.configuration.news_per_page
    else
      news_per_page = 10
    end

    @newscast = News.all.desc(:_id).paginate(page: params[:page], per_page: news_per_page)

    path = url_for(controller: controller_name, action: action_name, only_path: 'true').gsub /^\//, ''
    @page = Page.where(slug: path).first

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @newscast }
    end
  end

  def admin_index
    @newscast = News.all.desc(:_id)

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @newscast }
    end
  end

  # GET /news/1
  # GET /news/1.json
  def show
    @news = News.where(name_translit: params[:id]).first

    raise ActionController::RoutingError.new('Not Found') unless @news.present?

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @news }
    end
  end

  # GET /news/new
  # GET /news/new.json
  def new
    @news = News.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @news }
    end
  end

  # GET /news/1/edit
  def edit
    @news = News.find(params[:id])
  end

  # POST /news
  # POST /news.json
  def create
    @news = News.new(params[:news])

    respond_to do |format|
      if @news.save
        format.html { redirect_to edit_news_path(@news), notice: 'Новость успешно добавлена.' }
        format.json { render json: @news, status: :created, location: @news }
      else
        format.html { render action: "new" }
        format.json { render json: @news.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /news/1
  # PUT /news/1.json
  def update
    @news = News.find(params[:id])

    respond_to do |format|
      if @news.update_attributes(params[:news])
        format.html { redirect_to edit_news_path(@news), notice: 'Новость успешно обновлена' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @news.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /news/1
  # DELETE /news/1.json
  def destroy
    @news = News.find(params[:id])
    @news.destroy

    respond_to do |format|
      format.html { redirect_to news_index_path, notice: 'Новость успешно удалена' }
      format.json { head :no_content }
    end
  end
end
