class RobotsTxtController < ApplicationController
  include ApplicationHelper

  def index
    render text: text_zone('robots.txt'), content_type: 'text/plain'

  end
end
