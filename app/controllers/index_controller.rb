# coding: utf-8
class IndexController < ApplicationController
  def index
    @page = Page.root.first
    @newscast = News.desc(:_id).limit(4)
  end
end