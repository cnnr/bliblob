class TextZonesController < ApplicationController
  before_filter :authenticate_user!

  # GET /text_zones
  # GET /text_zones.json
  def index
    @text_zones = TextZone.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @text_zones }
    end
  end

  # GET /text_zones/1
  # GET /text_zones/1.json
  def show
    @text_zone = TextZone.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @text_zone }
    end
  end

  # GET /text_zones/new
  # GET /text_zones/new.json
  def new
    @text_zone = TextZone.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @text_zone }
    end
  end

  # GET /text_zones/1/edit
  def edit
    @text_zone = TextZone.find(params[:id])
  end

  # POST /text_zones
  # POST /text_zones.json
  def create
    @text_zone = TextZone.new(params[:text_zone])

    respond_to do |format|
      if @text_zone.save
        format.html { redirect_to text_zones_url, notice: 'Text zone was successfully created.' }
        format.json { render json: @text_zone, status: :created, location: @text_zone }
      else
        format.html { render action: "new" }
        format.json { render json: @text_zone.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /text_zones/1
  # PUT /text_zones/1.json
  def update
    @text_zone = TextZone.find(params[:id])

    respond_to do |format|
      if @text_zone.update_attributes(params[:text_zone])
        format.html { redirect_to text_zones_url, notice: 'Text zone was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @text_zone.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /text_zones/1
  # DELETE /text_zones/1.json
  def destroy
    @text_zone = TextZone.find(params[:id])
    @text_zone.destroy

    respond_to do |format|
      format.html { redirect_to text_zones_url }
      format.json { head :no_content }
    end
  end
end
