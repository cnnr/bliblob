# coding: utf-8
class FileManagementController < ApplicationController
  before_filter :authenticate_user!
  skip_before_filter :verify_authenticity_token, only: [:picture_upload, :attachment_upload]

  include Ckeditor::Helpers::Controllers

  def index
    @pictures = Ckeditor.picture_model.find_all(ckeditor_pictures_scope)
    @attachments = Ckeditor.attachment_file_model.find_all(ckeditor_attachment_files_scope)
  end

  def images
    json_images = []

    @images = Ckeditor.picture_model.find_all(ckeditor_pictures_scope)
    @images.each do |image|
      json_images << {
          thumb: image.data.thumb.url,
          image: image.data.url
      }
    end

    respond_to do |format|
      format.json { render :json => json_images, :layout => false }
    end
  end

  def picture_upload
    if (data = params[:image][:image]).present?
      @picture = Ckeditor::Picture.create(data: data)

      render text: "<img src=\"#{@picture.data.url}\" />", layout: false
    else
      render text: "", layout: false
    end
  end

  def attachment_upload
    if (data = params[:file]).present?
      @file = Ckeditor::AttachmentFile.create(data: data)
      render text: "<a href=\"#{@file.data.url}\">#{@file.data.filename}</a>", layout: false
    else
      render text: "", layout: false
    end
  end

  def destroy
    @asset = Ckeditor::Asset.find(params[:id])

    filename = "#{Rails.root}/public/#{@asset.url}"
    File.delete(filename) if File.exist? filename

    @asset.destroy

    respond_to do |format|
      format.html { redirect_to file_management_index_path }
      format.json { head :no_content }
    end
  end
end
