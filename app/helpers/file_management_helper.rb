# coding: utf-8

module FileManagementHelper
  def kilobytes(size)
    "#{(size / 1024.0).round(2)} Кб"
  end
end
