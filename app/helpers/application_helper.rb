module ApplicationHelper
  def transliterate(str)
    Russian::transliterate(str).gsub(' ', '_').downcase
  end

  def title
    meta_title = get_current_object_meta_field 'title'

    #if @current_object.present?  && @current_object.name != meta_title
    if controller_name == 'catalog'
      [text_zone('title_prefix'), meta_title, text_zone('title_postfix')].reject(&:empty?).join(' ')
    else
      meta_title
    end
  end

  def keywords
    get_current_object_meta_field 'keywords'
  end

  def description
    get_current_object_meta_field 'description'
  end

  def current_object
    @current_object ||= if defined? @category
      @category
    elsif defined? @page
      @page
    elsif defined? @news
      @news
    end
  end

  def get_current_object_meta_field(meta_field)
    if current_object.present?
      current_object.meta.present? && (field = current_object.meta[meta_field]).present? ? field : current_object.name
    else
      ''
    end
  end

  def counters
    Counter.enabled.distinct(:source).join("\n")
  end

  def text_zone(name)
    TextZone.public.where(name: name).distinct(:source).first || ""
  end

  def suggests
    Category.distinct(:name) + Page.distinct(:name)
  end

  def admin_scope?
    request.path.starts_with?('/admin') || (controller_name == 'registrations' && action_name == 'edit')
  end

  #def is_admin_controller?
  #  ([:pages, :registrations].include?(controller_name.to_sym) && [:index, :edit, :create, :update].include?(action_name.to_sym)) ||
  #      [:products, :categories, :counters].include?(controller_name.to_sym)
  #end
end
