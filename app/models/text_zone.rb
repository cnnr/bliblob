class TextZone
  include Mongoid::Document
  field :name, :type => String
  field :description, :type => String
  field :enabled, :type => Boolean, default: true
  field :source, :type => String

  validates_uniqueness_of :name

  scope :public, where(enabled: true)
end
