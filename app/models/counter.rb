class Counter
  include Mongoid::Document
  field :name, :type => String
  field :enabled, :type => Boolean, :default => true
  field :source, :type => String

  scope :enabled, where(enabled: true)
end
