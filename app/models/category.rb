# coding: utf-8
class Category
  include Mongoid::Document
  include Extensions::Translit
  include Extensions::Meta
  include Extensions::OrderByInteger
  include Extensions::Cleanup
  include Extensions::Path
  #include Extensions::UpdateRoutes
  include Extensions::Teaser

  field :description
  cleanup_markup_for :description

  field :features, type: Array, default: []

  mount_uploader :picture, CategoryPictureUploader

  belongs_to :category
  scope :first_level, where(category_id: nil)

  has_many :categories

  alias_method :parent, :category
  alias_method :children, :categories

  def destroy
    if (children || []).size.zero?
      super
    else
      errors.add :base, "Невозможно удалить категорию, потому что у нее есть дети"
      false
    end
  end

  alias_method :old_breadcrumbs, :breadcrumbs

  def breadcrumbs
    old = old_breadcrumbs
    old.pop

    old << old_breadcrumbs.last.collect do |str|
      str.sub '/', ''
    end

    old
  end
end
