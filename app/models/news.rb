# coding: utf-8
class News
  include Mongoid::Document
  include Extensions::Translit
  include Extensions::Meta
  include Extensions::Cleanup

  field :name, type: String
  field :teaser, type: String
  field :text, type: String

  def description
    "#{teaser} #{text}"
  end

  cleanup_markup_for :text

  field :date, type: String, default: proc { I18n.l(Date.today) }
  field :real_date, type: Time

  validates_presence_of :date
  validates_presence_of :real_date, message: 'Неправильный формат даты, пожалуйста, укажите дату в виде число.месяц.год'
  validates_uniqueness_of :name, :name_translit

  default_scope order_by([[:real_date, :desc], [:created_at, :desc]])

  field :external_link, type: String
  field :file_link, type: String
  field :media_name, type: String
  field :picture_link, type: String

  attr_accessor :file
  attr_accessor :picture

  before_validation do
    if self.picture.present?
      self.picture_link = Ckeditor::Picture.create(data: self.picture).data.url
    end

    if self.file.present?
      self.file_link = Ckeditor::AttachmentFile.create(data: self.file).data.url
    end

    begin
      self.real_date = Date.parse self.date
    rescue

    end
  end
end
