# coding: utf-8
class OrderSettings
  include Mongoid::Document

  FIELDS = Category.first_level.distinct(:name).append('Другое')

  field :data, class: Hash, default: {}

  class << self
    def instance
      self.send(:first) || self.new
    end

    def emails(field)
      (instance.data[field] || "").lines.collect {|line| line.split(',')}.flatten.collect(&:strip)
    end
  end
end