class Page
  include Mongoid::Document
  include Extensions::Meta
  include Extensions::OrderByInteger
  include Extensions::Translit
  include Extensions::Cleanup
  include Extensions::Path
  #include Extensions::UpdateRoutes
  #include Extentions::AltText

  field :description, :type => String

  cleanup_markup_for :description
  #alt_text_for :description

  has_many :pages, :dependent => :nullify
  belongs_to :page

  alias :parent :page
  alias :children :pages
#  alias :parent= :page=
#  alias :parent_id :page_id

  scope :not_root, where(:name_translit.nin => ['/'])
  scope :root, where(:name_translit => '/')
  scope :first_level, where(:page_id => nil)

  def root?
    self.name_translit == '/'
  end
end
