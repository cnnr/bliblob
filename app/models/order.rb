# coding: utf-8
class Order
  include Mongoid::Document
  include Mongoid::Timestamps

  default_scope order_by([[:created_at, :asc]])

  FIELDS = {name: 'Имя',
            company: 'Компания',
            email: 'E-mail',
            phone: 'Телефон',
            order_type: 'Продукция',
            text: 'Текст заявки'
  }

  REQUIRED_FIELDS = [:name, :phone]

  FIELDS.each do |name, title|
    field name, type: String
    validates_presence_of name, message: "Поле «#{title}» не может быть пустым." if REQUIRED_FIELDS.include? name
  end

  after_save do
    OrderMailer.order_email(self).deliver
  end
end
