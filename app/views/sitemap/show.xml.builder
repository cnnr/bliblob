# this procudes the <?xml ... ?> tag at the start of the document
#   note: this is different to calling builder normally as the <?xml?> tag
#         is very different to how you'd write a normal tag!
xml.instruct! :xml, :version => '1.0', :encoding => 'UTF-8'

# create the urlset
xml.urlset :xmlns => 'http://www.sitemaps.org/schemas/sitemap/0.9' do
  @urls << text_zone('sitemap.xml').lines.to_a
  @urls.flatten!

  #TODO: fix with default hostname

  @urls.each do |url|
    xml.url do
      xml.loc "http://#{request.host_with_port}/#{url}"
    end
  end

  @pages.each do |slug|
    xml.url do
      xml.loc "http://#{request.host_with_port}/#{slug}"
    end
  end


  if respond_to? :custom_new_url
    @news.each do |slug|
      xml.url do
        xml.loc custom_news_url(slug)
      end
    end
  end

  @categories.each do |slug|
    xml.url do
      xml.loc "http://#{request.host_with_port}/#{@prefix}#{slug}"
    end
  end
end