require "spec_helper"

describe IndexController do
  describe "routing" do

    it "routes to #index" do
      get("/").should route_to("index#index")
    end

  end
end
