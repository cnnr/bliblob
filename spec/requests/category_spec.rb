# coding: utf-8
require 'spec_helper'

describe CategoriesController do
  before(:each) do
    @subcategory = Factory(:subcategory)
    @category = @subcategory.parent

    if (@prefix = Rails.application.routes.catalog_prefix).present?
      @prefix = @prefix.present? ? "#{@prefix}/" : ''
    end

    get "#{@prefix}#{@category.slug}"
  end

  describe "GET /category" do
    it "should return 200" do
      response.status.should be(200)
    end

    it "should render category name" do
      response.body.include?("<h1>#{@category.name}</h1>").should be true
    end

    it "should render category description" do
      response.body.include?(@category.description).should be true
    end

    it "should render subcategory name" do
      response.body.include?(@subcategory.name).should be true
    end
  end

  describe "GET /category/subcategory" do
    it "200" do
      get "#{@prefix}#{@subcategory.slug}"
      response.status.should be(200)
    end
  end

  describe "GET /prefix" do
    it "200" do
      Rails.application.routes.class_eval do
        attr_accessor :catalog_prefix
      end

      Rails.application.routes.catalog_prefix = FactoryGirl.generate(:random_string)
      Rails.application.routes_reloader.reload!

      get Rails.application.routes.catalog_prefix

      response.body.include?("<h1>Каталог</h1>").should be true
      response.body.include?(@category.name).should be true
    end
  end
end
