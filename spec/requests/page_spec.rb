# coding: utf-8
require 'spec_helper'

describe PagesController do
  before(:each) do
    @page = Factory(:page)
    get @page.slug
  end

  describe "GET /page" do
    it "should return 200" do
      response.status.should be(200)
    end

    it "should render page name" do
      response.body.include?("<h1>#{@page.name}</h1>").should be true
    end

    it "should render page description" do
      response.body.include?(@page.description).should be true
    end
  end

  describe "GET /page/subpage" do
    it "200" do
      @subpage = Factory(:subpage)
      get @subpage.slug
      response.status.should be(200)
    end
  end

  describe "metadata check" do
    before :each do
      class_eval do
        include ApplicationHelper

        def controller_name
          'pages'
        end
      end
    end

    it "page should contain title" do
      contains_title = response.body.include?("<title>#{title}</title>")
      contains_title.should be true
    end

    it "page should contain description" do
      contains_description = response.body.include?("<meta name=\"description\" content=\"#{description}\">")
      contains_description.should be true
    end

    it "page should contain keywords" do
      contains_keywords = response.body.include?("<meta name=\"keywords\" content=\"#{keywords}\">")
      contains_keywords.should be true
    end
  end

end
