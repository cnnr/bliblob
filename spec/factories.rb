# coding: utf-8
require 'factory_girl'

FactoryGirl.define do
  Chars = 'qwertyuiopasdfghjklzxcvbnm'
  #'qwertyuiop[]asdfghjkl;\'\zxcvbnm,./`"±!@#$%^&*()_+?~|}{:=-0987654321йцукенгшщзхъфывапролджэячсмитьбюё'

  random_string = ->{
    40.times.collect do
      Chars.chars.to_a.sample
    end.join
  }

  sequence :random_string do
    random_string.call
  end

  factory :user do
    name 'Test User'
    email 'user@test.com'
    password 'please'
  end

  factory :page do
    name &random_string
    description &random_string
  end

  factory :subpage, class: Page do
    name &random_string
    description &random_string
    page
  end

  factory :subsubpage, class: Page do
    name &random_string
    description &random_string
    association :page, factory: :subpage
  end

  factory :category do
    name &random_string
    description &random_string
  end

  factory :subcategory, class: Category do
    name &random_string
    description &random_string
    category
  end
end
