# coding: utf-8
require 'spec_helper'

describe Extensions::Path do
  before(:each) do
    class Page
      include Mongoid::Timestamps
    end

    @subsubpage = Factory(:subsubpage)
    @subpage = @subsubpage.parent
    @page = @subpage.parent
  end

  describe "slugs" do
    it "first-level" do
      @page.slug.should == @page.name_translit
    end

    it "second-level" do
      @subpage.slug.should == "#{@page.slug}/#{@subpage.name_translit}"
    end

    it "third-level" do
      @subsubpage.slug.should == "#{@subpage.slug}/#{@subsubpage.name_translit}"
    end

    it "should be updated if name_translit changes" do
      @page.name_translit = "blaaargh"
      @page.save!

      @page.reload

      @page.slug.should == "blaaargh"
    end

    it "children slugs update recursively if parent slug changes" do
      @page.name = "blaaargh"
      @page.name_translit = "blaaargh"
      @page.save!

      [@page, @subpage, @subsubpage].each &:reload

      @page.slug.should == "blaaargh"
      @subpage.slug.should == "blaaargh/#{@subpage.name_translit}"
      @subsubpage.slug.should == "blaaargh/#{@subpage.name_translit}/#{@subsubpage.name_translit}"
    end

    it "does not update children slugs if parent slug does not change" do
      @page.updated_at.eql?(@page.created_at).should be true

      @page.name = "blaaargh"
      sleep(1) && @page.save!

      [@page, @subpage, @subsubpage].each &:reload

      @page.slug.should_not == "blaaargh"
      @page.name.should == "blaaargh"

      (@page.updated_at > @page.created_at).should be true
      [@subpage.reload.updated_at, @subsubpage.reload.updated_at].should == [@subpage.created_at, @subsubpage.created_at]
    end
  end
end
