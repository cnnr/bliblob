# coding: utf-8
require 'spec_helper'

describe Extensions::OrderByInteger do
  before(:each) do
    class Test::OrderedItem
      include Mongoid::Document
      include Extensions::OrderByInteger

      field :name, type: String
    end

    @a = Test::OrderedItem.create(:name => "Ah")
    @b = Test::OrderedItem.create(:name => "Be")
    @c = Test::OrderedItem.create(:name => "Ci")
  end

  describe "Should really sort" do
    it "for name" do
      Test::OrderedItem.all.should == [@a, @b, @c]
    end

    it "for description" do
      [@c, @a, @b].each_with_index do |item, index|
        item.order_number = index
        item.save
      end
      Test::OrderedItem.all.should == [@c, @a, @b]
    end

  end

end
