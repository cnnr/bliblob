# coding: utf-8
require 'spec_helper'

describe Extensions::Cleanup do
  before(:each) do
    class Test::Blank
      include Mongoid::Document
      include Extensions::Cleanup

      field :name, type: String, default: <<-NAME
        <p style="font-size: 7px">paragraph</p>
        <span style="hurrdurr"><font size="12">font</font> span</span>
      NAME

      cleanup_markup_for :name
    end

    @blank = Test::Blank.new
    @name = @blank.name
  end

  describe "Should enable cleanup for field" do
    it "Extensions::Cleanup.cleanup should be available" do
      Extensions::Cleanup.respond_to?(:cleanup).should be true
    end

    it "should respond to cleanup_markup_for" do
      @blank.class.respond_to?(:cleanup_markup_for).should be true
    end

    it "should leave attribute intact if enable_cleanup = false (by default)" do
      @blank.save
      @blank.name.should eq @name
    end

    it "should fix attribute if enable_cleanup = true" do
      @blank.enable_cleanup = true
      @blank.save

      @blank.name.should_not eq @name
      @blank.name.should == <<-NAME
        <p>paragraph</p>
        font span
      NAME
    end
  end
end
